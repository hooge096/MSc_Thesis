This project contains the means to extract biosysnthetic gene clusters from fungal genomes.
The script should be called using the run.py file. From there, the other scripts will be called to complete their
function. More information on each script can be found in the header docstring of that script.