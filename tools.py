#!/usr/bin/env python
"""
Author: Koen Hoogendoorn
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: Contains all the functions that run the tools from the command line.

Instructions:
see run.py

"""

# IMPORTS
from __future__ import division

import funclib
from funclib import anchor_genes_to_file, get_cassis_locations, get_antismash_locations, \
     improve_network_file, parse_config, commands_to_file
from Bio import SeqIO
from shutil import copy2
import subprocess as s
import run
import converters
import os

# Functions


@funclib.timeit
def run_busco(input_file, busco_path=""):
    """
    Sends the call to the linux command line to run the BUSCO analysis
    :param input_file: str, path and name of the input file for the busco analysis
    :param busco_path: str, path to the BUSCO.py file
    :return: busco_path: str,
    """

    # Prepares the BUSCO arguments and writes the command to a file
    input_file = converters.convert_files(input_file, "busco")
    arguments, out_folder, busco_path, temp = parse_config(input_file, "BUSCO", busco_path)
    cmd = "python3 {} -o {} -in {} {}".format(busco_path, out_folder, input_file, arguments)
    print cmd
    commands_to_file(cmd)
    print '='*50

    # Runs BUSCO
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError as error:
        print "BUSCO Crashed:"
        print error.output

    return busco_path


@funclib.timeit
def run_antismash(input_file, version):
    """
    Runs antiSMASH from the command line using a genbank file as input, grabbing arguments from a config file
    :param input_file: str, full path of the genbank input file.
    :param version: str, the version of antismash that is to be used.
    :return: N/A
    """

    # Determines which version of antiSMASH to use (Stable/Dev)
    if version == "dev":
        command = 'run_from_docker'
    else:
        command = 'run_antismash'

    # Prepares the arguments for antiSMASH
    arguments, out_folder, temp, temp2 = parse_config(input_file, "antiSMASH")
    cmd = "{} {} {} {}".format(command, input_file, out_folder, arguments)
    print cmd
    commands_to_file(cmd)

    # Runs the antiSMASH command and, if succesfull, writes the found anchor genes to a file
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError as error:
        print "antiSMASH Crashed for some reason:"
        print error.output
    else:
        anchor_genes_to_file(out_folder, input_file)


@funclib.timeit
def run_cassis(input_file):
    """
    Runs CASSIS from the command line using a genbank file which is converted to a gff3 file on the fly.
    :param input_file: str, full path of the genbank input file
    :return: N/A
    """

    # Prepares arguments and input files for the run
    in_fasta, in_gff3 = converters.convert_files(input_file, 'cassis')
    arguments, out_folder, temp, temp2 = parse_config(input_file, "CASSIS")
    outdir = out_folder+os.sep+os.path.splitext(os.path.split(input_file)[1])[0]
    if not os.path.isdir(outdir):
        s.check_call("mkdir -p {}".format(outdir), shell=True)
    anchorgenes = []
    with open(os.path.splitext(input_file)[0]+"_anchor_genes.txt", "r") as anchor_file:
        for line in anchor_file:
            if line.strip() != "":
                anchorgenes.append(line.strip())

    # Prepares commands for every single anchor gene and prints them to a file
    for anchorgene in anchorgenes:
        cmd = "cassis --annotation {} --genome {} --dir {} --anchor {} {}".format(in_gff3, in_fasta, outdir,
                                                                                  anchorgene, arguments)
        cmd += "| sed -n -e '/(8)/,$p' "
        cmd += "> {}.txt ".format(outdir+os.sep+anchorgene)
        print cmd
        commands_to_file(cmd)

        # Runs CASSIS with the previously set parameters
        try:
            s.check_call(cmd, shell=True)
        except s.CalledProcessError as error:
            print error.output
            commands_to_file("Previous command failed!")
        print "="*50


@funclib.timeit
def compare_cassis_antismash(inputfile, outdir="", warning=False):
    """
    Compares the CASSIS and antiSMASH start and endlocations and outputs it to a readable format.
    :param inputfile: str, location of a genome annotation in genbank format
    :param outdir: str, the path where the output should be created
    :param warning: bool, ignore or use CASSIS warnings
    :return: N/A
    """
    species = os.path.splitext(os.path.split(inputfile)[-1])[0]
    print species
    cassis_output = (parse_config("placeholder", "CASSIS"))[1]
    antismash_output = (parse_config("placeholder", "antiSMASH"))[1]
    genelist = []
    final_gbk = ""
    records = list(SeqIO.parse(inputfile, "genbank"))

    # Grabs the antiSMASH results corresponding to the genome annotation
    for clusterfile in os.listdir(antismash_output+os.sep+species):
        if clusterfile.endswith(".final.gbk"):
            final_gbk = clusterfile
    antismash_records = list(SeqIO.parse(antismash_output+os.sep+species+os.sep+final_gbk, "genbank"))

    # Builds a list of all genes in the genbank genome annotation
    for record in records:
        for feature in record.features:
            if feature.type == "gene":
                try:
                    genelist.append(feature.qualifiers["locus_tag"][0])
                except KeyError:
                    genelist.append(feature.qualifiers["gene"][0])

    # Writes the comparison to a file
    with open(outdir.rstrip(os.sep)+os.sep+species+".txt", "w") as out_handle:
        headers = "Anchor_gene\tAnti_start\tCassis_start\tStart_dif\t" \
                  "Anti_stop\tCassis_stop\tStop_dif\tlen_Anti\tlen_CASSIS\n"
        out_handle.write(headers)
        for start, stop, len_cassis, anchor in get_cassis_locations(cassis_output+os.sep+species, warning):
            antistart, antistop, len_anti, comp = get_antismash_locations(antismash_output + os.sep + species,
                                                                          anchor, antismash_records)
            if "+" in start:
                start = sorted(start.split("+"))[0]
            if "+" in stop:
                stop = sorted(stop.split("+"), reverse=True)[0]

            # If a gene is somehow not in the genelist, it at least won't crash
            try:
                startdif = genelist.index(antistart)-genelist.index(start)
            except ValueError:
                startdif = "N/A"
            try:
                stopdif = genelist.index(antistop)-genelist.index(stop)
            except ValueError:
                stopdif = "N/A"

            # Builds the line to be printed to the file
            if comp:
                line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(anchor, antistop, stop, stopdif, antistart, start,
                                                                     startdif, len_anti, len_cassis.strip())
            else:
                line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(anchor, antistart, start, startdif, antistop, stop,
                                                                     stopdif, len_anti, len_cassis.strip())
            out_handle.write(line)


@funclib.timeit
def run_bigscape(antismash_results):
    """
    Runs BiGSCAPE from the command line. Requires antiSMASH results to be used as input.
    :param antismash_results: str, path to a folder containing antiSMASH results (subfolders allowed)
    :return:
    """

    # Prepare arguments required for BiG-SCAPE run
    arguments, out_folder, temp, bigscape_path = parse_config(antismash_results, "BiGSCAPE")
    cmd = "python {} -s -i {} -o {} {} ".format(bigscape_path, antismash_results, out_folder, arguments)

    # Runs BiG-SCAPE
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError as error:
        print error.output
        print "something went wrong"

    # Immediately improve the network files generated by BiG-SCAPE
    else:
        for elem in os.listdir(out_folder):
            if elem.startswith("networks"):
                networkfolder = elem
                for networkfile in os.listdir(out_folder+os.sep+networkfolder):
                    improve_network_file(networkfile, parse_config("placeholder", "antiSMASH")[1])


@funclib.timeit
def make_multigeneblast_db(name, multigeneblast_location, datafolder):
    """
    Makes a blast database of the specified clusters in gbk format
    :param name: str, name of the database
    :param multigeneblast_location: str, path to the mgb installation
    :param datafolder: str, path to your genbank files
    :return: str, database name
    """
    os.chdir(multigeneblast_location)
    cmd = "python makedb.py {} {}".format(name, datafolder)
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError as error:
        print error.output
        print "Database creation failed"
    else:
        databaselocation = os.path.split(multigeneblast_location.rstrip(os.sep))[0]
        for files in os.listdir(databaselocation):
            if name in files:
                copy2(os.path.join(databaselocation, files), os.path.join(multigeneblast_location, files))

    return name


@funclib.timeit
def clusterblast(inputfile, database, multigeneblast_location):
    """
    runs multigeneblast on a given genbank file and a blast database
    :param inputfile: str, gbk filepath
    :param database: str, name of the database (to be created manually using multigeneblast)
    :param multigeneblast_location: str, path to the folder where multigeneblast is installed
    :return: N/A
    """
    start = 1
    end = 10000000
    records = list(SeqIO.parse(inputfile, "genbank"))

    # Attempts to get the size of the cluster to use for the query range
    for feature in records[0].features:
        if feature.type == "cluster":
            start = feature.location.nofuzzy_start
            end = feature.location.nofuzzy_end
            break

    # Sets appropriate arguments for multigeneblast
    outfile = os.path.splitext(inputfile.split(os.sep)[-1])[0]
    outfile = outfile.replace(".", "_")  # multigeneblast doesn't like (crashes on) periods in folder names
    os.chdir(multigeneblast_location)
    cmd = "python multigeneblast.py -in {} -out {} -db {} -from {} -to {}".format(inputfile, outfile, database,
                                                                                  start, end)
    print cmd
    print "="*50

    # Runs multigeneblast
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError:
        print "Multigeneblast failed to run for cluster '{}'.".format(inputfile)


if __name__ == "__main__":
    print "This is a module which only contains functions, and cannot be called on its own. use run.py!"
