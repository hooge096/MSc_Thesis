#!/usr/bin/env python
"""
Author: Koen Hoogendoorn
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: contains all accessory functions, mainly involved in converting formats into other formats.

Instructions:
see run.py
"""

# Imports
from Bio import SeqIO
from BCBio import GFF
from Bio.Alphabet import generic_dna
import os.path

# Classes & Functions


def convert_gb_fasta(infile):
    """
    Extracts the nucleotide sequences from the ORIGIN feature of a genbank file
    :param infile: str, name of the input file
    :return: str, either the accession of the fasta record or a line of its sequence
    """
    outfile = os.path.splitext(infile)[0]+'.fasta'
    if not os.path.isfile(outfile):
        SeqIO.convert(infile, "genbank", outfile, "fasta")
        print "converted file: {}".format(outfile)
    else:
        print "Converted file '{}' already exists".format(outfile)
    return outfile


def convert_gb_gff3(infile):
    """
    Converts a genbank file into a gff3 file with all the available information.
    :param infile:
    :return:
    """
    outfile = os.path.splitext(infile)[0]+"_genefeatures.gff3"
    if not os.path.isfile(outfile):
        records = list(SeqIO.parse(infile, "genbank"))
        double_genes = []
        outrecords = []
        counter = 0
        for index1, record in enumerate(records):
            indexlist = []
            for index, feature in enumerate(record.features):
                if feature.type not in ["gene", "source"]:
                    indexlist.append(index)
                elif feature.type == "gene":
                    try:
                        feature.qualifiers['ID']
                    except KeyError:
                        try:
                            if feature.qualifiers['locus_tag'] not in double_genes:
                                feature.qualifiers['ID'] = feature.qualifiers['locus_tag']
                            else:
                                feature.qualifiers['ID'] = [str(counter)]
                                counter += 1
                        except KeyError:
                            feature.qualifiers['ID'] = [str(counter)]
                            counter += 1
                        else:
                            double_genes.append(feature.qualifiers['locus_tag'])

            indexlist.sort(reverse=True)
            for element in indexlist:
                records[index1].features.pop(element)
            outrecords.append(records[index1])

        with open(outfile, 'w') as out_handle:
            GFF.write(outrecords, out_handle)
        print "converted file: {}".format(outfile)
    else:
        print "Converted file '{}' already exists".format(outfile)
    return outfile


def gff_to_gb(gff_file, fasta_file):
    """
    Converts a gff file along with its reference fasta file to a file in genbank format
    :param gff_file: str, path to gff file
    :param fasta_file: str, path to reference fasta file
    :return: str, output filename + path
    """
    outfile = "{}.gb".format(os.path.splitext(gff_file))[0]
    fasta_input = SeqIO.to_dict(SeqIO.parse(fasta_file, "fasta", generic_dna))
    gff_iterator = GFF.parse(gff_file, fasta_input)
    SeqIO.write(gff_iterator, outfile, "genbank")
    return outfile


def convert_files(in_file, out_format):
    """
    If possible, converts a given file to a given format
    :param in_file: str, the location of the input file
    :param out_format: str, the format it has to be converted to
    :return: tuple (str), name of the output files (if created)
    """
    if out_format == "busco" and os.path.splitext(in_file)[1] in [".gb", ".gbk", ".genbank"]:
        print "Converting {} to fasta format".format(os.path.split(in_file)[1])
        in_name_fasta = convert_gb_fasta(in_file)
        return in_name_fasta

    if out_format == "cassis" and os.path.splitext(in_file)[1] in [".gb", ".gbk", ".genbank"]:
        in_name_fasta = convert_gb_fasta(in_file)
        in_name_gff3 = convert_gb_gff3(in_file)
        return in_name_fasta, in_name_gff3

    else:
        return in_file


if __name__ == "__main__":
    print "This is a module which only contains functions, and cannot be called on its own. Use run.py!"
