#!/usr/bin/env python
"""
Author: Koen Hoogendoorn
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: Contains all accessory functions.

Instructions:
see run.py

"""

# IMPORTS
from __future__ import division

import argparse
import os.path
import time
from datetime import datetime, time
from Bio import SeqIO
import subprocess as s
from shutil import copy2
import config
import os
import re


def commands_to_file(cmd):
    """
    Writes the commands issued through the subprocess module to a file for later reference
    :param cmd: str, The command to be printed
    :return: N/A
    """
    with open("commands.txt", "a") as out_handle:
        out_handle.write(datetime.now().strftime('[%Y-%m-%d||%H:%M:%S]'))
        out_handle.write("\t" + cmd + "\n")


def timeit(f):
    """
    Writes the runtimes of functions to a file, and prints them on the screen
    :param f: a function
    :return: that function's output
    """

    def wrap(*args):
        insignificant_runtime = 0.1  # prevents an overload
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        runtime = time2 - time1
        runtime_string = '{} function took {} seconds'.format(f.func_name, runtime)
        if runtime > insignificant_runtime:
            commands_to_file(runtime_string)
        return ret

    return wrap


@timeit
def anchor_genes_to_file(out_folder, input_file):
    """
    writes all anchor genes (genes with biosynthetic activity) to a file.
    :param out_folder: The antiSMASH output folder
    :param input_file: str, location of the antiSMASH input file
    :return: N/A
    """

    # Finds the file containing all predicted clusters in gbk format
    finalgbk = s.check_output(
        "find {} -name *.final.gbk".format(out_folder + os.sep + (os.path.splitext(os.path.split(input_file)[1])[0])),
        shell=True)
    finalgbk = finalgbk.strip()
    print finalgbk

    # Specifies two locations to write the anchor genes file to
    file_dest = [out_folder + os.sep + os.path.splitext(os.path.split(input_file)[1])[0] +
                 os.sep + os.path.splitext(os.path.split(input_file)[1])[0], os.path.splitext(input_file)[0]]

    # Writes the anchor genes to two files, one in the directory of the antiSMASH results
    # and one in the directory of the genome annotation
    for dest in file_dest:
        print "Destination is: " + dest
        try:
            s.check_call("touch {}_anchor_genes.txt".format(dest), shell=True)
        except s.CalledProcessError:
            print "Could not create an anchor file at {}".format(os.path.split(dest)[0])
        else:
            with open("{}_anchor_genes.txt".format(dest), "w") as out_handle:
                locusgen = extract_locus(finalgbk)
                count = 0
                for locus in locusgen:
                    out_handle.write(locus + "\n")
                    count += 1
                print "Anchor file created at: {}".format(os.path.split(dest)[0])
                print '{} "Anchor" genes were found.'.format(count)
    print "=" * 50


@timeit
def extract_locus(filename):
    """
    Extracts loci with biosynthetic activity from the antiSMASH output (to be used in CASSIS analysis)
    :param filename: str, antiSMASH output file
    :return: str, locus tag of the gene with biosynthetic activity
    """
    records = list(SeqIO.parse(filename, "genbank"))
    print len(records)

    # Loops through all records in the final gbk file to extract all genes which antiSMASH considers "anchor" genes
    for record in records:
        print record.name
        for feature in record.features:
            if "sec_met" in feature.qualifiers.keys():
                for elem in feature.qualifiers["sec_met"]:
                    if 'Type' in elem:
                        yield feature.qualifiers["locus_tag"][0]


@timeit
def get_cassis_locations(cassis_folder, warning):
    """
    Extracts the startlocation, endlocation and the anchorgene of clusters predicted by CASSIS
    :param cassis_folder: str, the folder containing the output of one CASSIS run.
    :param warning: bool, changes the way warnings from CASSIS are handled
    :return: tup, containing the startlocation, endlocation and anchorgene as strings.
    """
    nextline = False
    extension = '.txt'

    # Selects CASSIS output files
    for textfile in os.listdir(cassis_folder):
        startlocation = "N/A"
        endlocation = "N/A"
        length = "N/A"
        if textfile.endswith(extension):
            anchor_gene = os.path.splitext(textfile)[0]
            count = 0
            with open(cassis_folder + os.sep + textfile, 'r') as in_handle:

                # Reads CASSIS output and finds start/end locations
                for line in in_handle:
                    if line.startswith("----------------"):
                        nextline = True
                    elif line.strip() == "":
                        nextline = False
                    elif nextline:
                        data = line.split("   ")
                        startlocation = (data[0].split("-")[0]).strip()
                        endlocation = (data[0].split("-")[1]).strip()
                        length = data[2]
                        count += 1

                        # If using the CASSIS warnings, only the first prediction will be used.
                        if warning:
                            nextline = False
            if count > 1:
                print("skipped the first {} predictions due to WARNINGS from CASSIS".format(count - 1))
            yield startlocation, endlocation, length, anchor_gene


@timeit
def get_antismash_locations(antismash_subfolder, anchor_gene, records):
    """
    Extracts the start and endlocation of a cluster predicted by antiSMASH using a given anchor gene.
    :param antismash_subfolder: str, Folder containing the antiSMASH results of one sample
    :param anchor_gene: str, Anchor gene of the cluster
    :param records: list, contains seqrecords objects from a genbank file
    :return:
    """
    startlocation = "N/A"
    endlocation = "N/A"
    length = "N/A"
    cluster_textfile = antismash_subfolder + os.sep + "geneclusters.txt"
    startcoords, stopcoords = (0, 0)

    # Grabs the start and end gene for a given anchor gene in a antiSMASH result folder
    with open(cluster_textfile, "r") as in_handle:
        for line in in_handle:
            cluster = line.split()[-2]
            genes = cluster.split(";")
            if anchor_gene in genes:
                startlocation = genes[0]
                endlocation = genes[-1]
                length = len(genes)

    # Maybe this never happens, but better safe than sorry. Accounts for reverse complement clusters
    for record in records:
        for feature in record.features:
            try:
                feat = feature.qualifiers["locus_tag"][0]
            except KeyError:
                continue
            else:
                if feat == startlocation:
                    startcoords = feature.location.nofuzzy_start
                if feat == endlocation:
                    stopcoords = feature.location.nofuzzy_end

    # Thinking about it now, this should never happen, but still.. better safe than sorry
    if startcoords > stopcoords:
        print anchor_gene, "\t", startcoords, "\t", stopcoords
        complement = True
    else:
        complement = False

    return startlocation, endlocation, length, complement


def improve_network_file(networkfile, main_antismash_dir):
    """
    Improves the BiG-SCAPE output by adding anchor genes and known compounds to the table as node attributes
    :param networkfile: str, path to a networkfile created by BiG-SCAPE
    :param main_antismash_dir: str, the antiSMASH output path
    :return: N/A
    """
    clusterdict = {}
    add_header_elems = ["knowncluster", "accession", "percentage", 'anchorgenes']
    species_dict = {"Chr1": "F poae", "CM000578": "F verticillioides", "HF679024": "F fujikuroi",
                    "HG970330": "F graminearum", "JH717896": "F oxysporum Fo47",
                    "CM000602": "F oxysporum f. sp. Lycopersici", "CM003198": "F pseudograminearum",
                    "HG323944": "F culmorum", "CM000574": "F graminearum broad"}

    with open(os.path.splitext(networkfile)[0] + "_improved.network", "w") as out_handle:
        with open(networkfile, 'r') as in_handle:
            for line in in_handle:

                # Determines the headers of the table and adds new headers corresponding to new data
                if line.startswith("clustername"):
                    data = line.split("\t")
                    headers = data[0:4] + ["species1"] + add_header_elems + data[4:6] + ["species2"]
                    headers += add_header_elems + data[6:]
                    out_handle.write("\t".join(headers))

                # Runs through all data in the network file and adds useful information where possible
                else:
                    data = line.split('\t')
                    cluster1 = data[0]
                    cluster2 = data[1]
                    species1 = "N/A"
                    species2 = "N/A"
                    species1_found = False
                    species2_found = False
                    for key in species_dict.iterkeys():
                        if cluster1.startswith(key):
                            species1 = species_dict[key]
                            species1_found = True
                        if cluster2.startswith(key):
                            species2 = species_dict[key]
                            species2_found = True

                    # Accounts for unknown species in the species dictionary
                    if not species1_found:
                        newkey = cluster1.split(".")[0]
                        species_dict[newkey] = raw_input("What species corresponds with accession {}:".format(newkey))
                        species1 = species_dict[newkey]
                    if not species2_found:
                        newkey2 = cluster2.split(".")[0]
                        species_dict[newkey2] = raw_input("What species corresponds with accession {}:".format(newkey2))
                        species2 = species_dict[newkey2]

                    # Prevents extracting info twice for the same cluster
                    if cluster1 not in clusterdict.keys():
                        clusterdict[cluster1] = extract_cluster_info(cluster1, main_antismash_dir)
                    if cluster2 not in clusterdict.keys():
                        clusterdict[cluster2] = extract_cluster_info(cluster2, main_antismash_dir)

                    # Writes the extra columns to the BiG-SCAPE network file
                    body = data[0:4] + [species1] + clusterdict[cluster1]
                    body += data[4:6] + [species2] + clusterdict[cluster2] + data[6:]
                    out_handle.write("\t".join(body))


def extract_cluster_info(clustername, mainfolder):
    """
    Grabs the anchor genes, the known compound (if available) and its accession and homology with the accession in MiBIG
    :param clustername: str, name of the cluster e.g. CM000578.cluster001
    :param mainfolder: str, antiSMASH output folder
    :return: list, contains the compound, accession, homology percentage and anchor genes for a specific cluster.
    """
    clusterhit = ""
    accession = ""
    percentage = ""
    anchor = ""

    # Finds the antiSMASH cluster file corresponding to the BiG-SCAPE node
    for root, dirs, files in os.walk(mainfolder):
        for name in files:
            if name == clustername + ".gbk":

                # Parses the network file and finds data for the knownclusterblast analysis
                records = list(SeqIO.parse(os.path.join(root, name), 'genbank'))
                for feature in records[0].features:
                    if feature.type == "cluster":
                        quals = feature.qualifiers
                        try:
                            bestcluster = quals["knownclusterblast"][0]
                        except KeyError:
                            pass
                        else:
                            accession = bestcluster.split("\t")[0][3:]
                            clusterhit = bestcluster.split("\t")[1].split(" ", 1)[0]
                            accession = accession.split("_")[0]
                            percentage = re.findall("[0-9]+%", bestcluster)[0]
                            print clusterhit, percentage

                    # Finds the "anchor" gene of the cluster according to antiSMASH
                    if feature.type == "CDS" and "sec_met" in feature.qualifiers.keys():
                        for elem in feature.qualifiers["sec_met"]:
                            if elem.startswith("Type:"):
                                anchor += feature.qualifiers["locus_tag"][0] + " "
                anchor = anchor.strip()
                print "Anchor genes:", anchor
    return [clusterhit, accession, percentage, anchor]


def get_tool_path(tool):
    """
    Tries to identify the correct path to the tool script in the users home directory and its subdirectories.
    :param tool: str, the tool the path is searched for
    :return: str, the path to the busco file.
    """

    homedir = s.check_output("echo $HOME", shell=True).strip()
    tool_path = s.check_output("find {} -name *{}*.py".format(homedir, tool), shell=True).split()

    # In case multiple matches are found, allow the user to choose the right file
    if len(tool_path) > 1 and type(tool_path) == list:
        print "=" * 50
        for counter, path in enumerate(tool_path):
            print str(counter) + "\t" + path
        print "=" * 50
        choice = 999
        while int(choice) not in range(counter + 1):
            choice = raw_input("choose the correct file from the list [0-{}]: ".format(counter))
            try:
                tool_path = tool_path[int(choice)]
            except ValueError:
                choice = 999
                print "That's not a valid number!"

    # In case one match is found, use that one
    elif len(tool_path) == 1:
        tool_path = tool_path[0]
        print tool_path
    else:
        print "{} file not found. Make sure you've installed it either in your home" \
              " directory or specify the correct path in the config file".format(tool)

    return tool_path


def parse_config(input_file, tool, busco_path="", bigscape_path=""):
    """
    Parses the options for every tool that is called in this script from a config file.
    :param input_file: str, input file used in the corresponding tool
    :param tool: str, identifier of the tool
    :param busco_path: path to busco
    :param bigscape_path: path to bigscape
    :return: tuple, contains the output path, the busco path and the command line arguments for the tool as strings.
    """
    arguments = ""
    outfolder = config.SCRIPT_PATH
    tool_section = False

    with open(config.SCRIPT_PATH + os.sep + "config.txt", 'r') as in_handle:
        for line in in_handle:

            # Determine right section of the config file
            if line.startswith("##") and tool in line:
                tool_section = True
            if line.startswith("##") and tool not in line:
                tool_section = False

            # Extract arguments from the config file and handles tool specific arguments
            if line.startswith("-") and tool_section:
                argument = line.split("=")[0]
                value = (line.split("=")[1]).rstrip()
                if argument == "name":
                    pass

                # Deals with paths of specific tools
                elif argument == "--BUSCO_PATH" and busco_path == "":
                    busco_path = value
                    if not os.path.isfile(busco_path):
                        print "'{}' is not a valid tool path!\nwe will now search for the " \
                              "correct file in your home directory".format(busco_path)
                        busco_path = get_tool_path("BUSCO")
                elif argument == "--BiGSCAPE_PATH" and bigscape_path == "":
                    bigscape_path = value
                    if not os.path.isfile(bigscape_path):
                        print "'{}' is not a valid tool path!\nI will now search for the " \
                              "correct file in your home directory".format(bigscape_path)
                        bigscape_path = get_tool_path("bigscape")

                # Miscellaneous exceptions to the simpler argument parsing
                elif argument == "--lineage":
                    arguments += argument + " " + busco_path.rsplit(os.sep, 1)[0] + os.sep + value + " "

                elif argument == "--outputfolder" or argument == "--outputdir":
                    if os.access(value, os.W_OK):
                        outfolder = value
                    elif value != "":
                        try:
                            s.check_call("mkdir -p {}".format(value), shell=True)
                            outfolder = value
                        except s.CalledProcessError:
                            print "could not create the specified folder, do you have acces?"
                            outfolder += os.sep + os.path.splitext(os.path.split(input_file)[1])[0]
                            print "now using {} as output folder".format(outfolder)
                            time.sleep(3)
                    else:
                        outfolder += os.sep + os.path.splitext(os.path.split(input_file)[1])[0]
                elif argument == "--dir" and tool == "CASSIS":
                    if value == "Default":
                        pass
                    else:
                        outfolder = value

                elif tool == "BUSCO":
                    outfolder = os.path.splitext(os.path.split(input_file)[1])[0]

                # Simple argument parsing
                elif value in ["False", "Default", "none"]:
                    pass

                elif value == "True":
                    arguments += argument + " "

                else:
                    arguments += argument + " " + value + " "

    return arguments, outfolder, busco_path, bigscape_path


@timeit
def extract_files(bigscape_out):
    """
    Extracts the filepaths of the antiSMASH clusters to be used in the multigeneblast
    :param bigscape_out: str, location to a BiG-SCAPE network file
    :return: str, path to antiSMASH cluster file corresponding with the node name of a single line
    """
    antismash_output = parse_config("placeholder", "antiSMASH")[1]
    os.chdir(antismash_output)
    nodeset = set()

    # Grabs all nodes from a BiG-SCAPE output file, removes duplicates to save time
    with open(bigscape_out) as in_handle:
        for line in in_handle:
            if not line.startswith('clustername1'):
                nodeset.add(line.split('\t')[0])
                nodeset.add(line.split('\t')[1])

    # All nodes in the network will get their appropriate antiSMASH cluster passed to multigeneblast
    for elem in nodeset:
        filepath = s.check_output("find {} -name {}.gbk".format(antismash_output, elem), shell=True)
        filepath = filepath.rstrip()
        yield filepath


@timeit
def move_clusters_to_folder():
    """
    Prepares a folder with all genbank accessions to make a blast database for the use of multigeneblast
    :return:
    """
    choice = ""
    antismash_output = parse_config("placeholder", "antiSMASH")[1]
    clusterfolder = os.path.split(antismash_output.rstrip(os.sep))[0] + os.sep + "multigeneblast_database_data"

    # Makes the directory to copy all the gbk files to
    try:
        os.makedirs(clusterfolder)
    except os.error:
        print "folder already exists or you have no access"
        while choice.upper not in ["Y", "N"]:
            choice = raw_input("Continue anyway? [Y/N]: ")
        if choice.upper() == 'Y':
            pass
        else:
            quit()

    # Copies all gbk files from various antiSMASH outputs into one folder
    for root, dirs, files in os.walk(antismash_output):
        for name in files:
            if name.endswith(".gbk") and not name.endswith("final.gbk"):
                genbankfile = os.path.join(root, name)
                try:
                    copy2(genbankfile, os.path.join(clusterfolder, name))
                except IOError:
                    print "Something went wrong copying {}".format(name)
                    print "The file has NOT been copied!"

    return clusterfolder


@timeit
def add_cluster_numbers(inputpath):
    """
    Rewrites the antiSMASH output so that cluster numbers are added to the blast database used for multigeneblast.
    THIS REQUIRES ALL GBK FILES TO BE IN ONE FOLDER!
    :param inputpath: str, path to all antiSMASH clusters in one folder
    :return: N/A
    """
    records = list
    # Species dictionary used in primary analysis. More entries can be added either on the fly or in advance.
    species_dict = {"Chr1": "Poae", "CM000578": "Verti", "HF679024": "Fujikuroi", "HG970330": "Graminearum",
                    "JH717896": "fo47", "CM000602": "lycopersici", "CM003198": "Pseudogram",
                    "HG323944": "Culmorum", "CM000574": "Gram_broad"}

    # Run through every genbank file and try to add species names and cluster numbers matching the antiSMASH numbering
    for gbkfile in os.listdir(inputpath):
        if gbkfile.endswith('.gbk') and not gbkfile.endswith('v2.gbk'):
            clusternumber = gbkfile[gbkfile.index("cluster") + 7:gbkfile.index("cluster") + 10]
            try:
                filename = species_dict[(gbkfile.split('.')[0])]
            except KeyError:
                newspecies = raw_input("Please provide a species name to accession {} "
                                       "(max 16 characters): ".format(gbkfile.split(".")[0]))
                if len(newspecies) > 16:
                    newspecies = newspecies[:16]
                species_dict[gbkfile.split(".")[0]] = newspecies
                filename = species_dict[(gbkfile.split('.')[0])]

            records = list(SeqIO.parse(inputpath + os.sep + gbkfile, "genbank"))
            for record in records:
                record.id = '{}_{}'.format(filename, clusternumber)
                record.name = '{}_{}'.format(filename, clusternumber)
                record.description = 'cluster {}'.format(clusternumber)

        # Attempts to write an improved file and, if succesfull, removes the original.
        try:
            SeqIO.write(records, inputpath + os.sep + os.path.splitext(gbkfile)[0] + "_v2.gbk", "genbank")
        except AttributeError:
            print "File: '{}' was not correctly modified. Cluster numbers will not be added.".format(gbkfile)
        else:
            os.remove(inputpath + os.sep + gbkfile)


def check_valid_file(arg):
    """
    Checks if a file is present with a given path
    :param arg: str, a (valid) path
    :return: str, the same path
    """

    if not os.path.exists(arg):
        raise argparse.ArgumentTypeError("{} is not a valid file!".format(arg))
    return arg


def grab_filenames(textfile):
    """
    Extracts valid file paths from a text file
    :param textfile: str, name of the textfile containing paths to input genome files
    :return: list, containing all valid files
    """
    files = []
    warning = []
    with open(textfile, 'r') as in_handle:
        for line in in_handle:
            if os.access(line.rstrip(), os.R_OK):
                files.append(line.rstrip())
            elif not line.rstrip():
                pass

            # If the file is not valid (doesn't exist), it gets added to a warning list
            else:
                warning.append(line)

    # Prints the list of invalid file with a prompt that asks the user to continue with the valid files or quit
    if len(warning) != 0:
        s.check_call("clear", shell=True)
        print '=' * 50 + '\n[WARNING] The following files were not detected or are not accessible:\n'
        for elem in warning:
            print elem
        print "=" * 50

        choice = ""
        while choice.upper() not in ["Y", "N"]:
            choice = raw_input("Continue? [Y/N]: ")
        if choice.upper() == "N":
            print "Exiting now"
            time.sleep(0.5)
            exit(0)
    return files
