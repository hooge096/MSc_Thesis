#!/usr/bin/env python
"""""
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: Framework for running various analysis tools involved in specialised metabolism gene clusters.

Dependencies:
-BUSCO v3


Instructions:
    Use --help for extended information on all the script's functionality
"""

# IMPORTS
from __future__ import division
from sys import argv
import os.path
import subprocess as s


def main(infile):
    files = []
    with open(infile) as filenames:
        for filename in filenames:
            files.append(filename.strip())

    for entry in files:
        output = os.path.splitext(os.path.split(entry)[1])[0]
        cmd = "python3 scripts/run_BUSCO.py -i {} -o {} -m geno -l sordariomyceta_odb9 -c 8".format(entry, output)
        try:
            print cmd
            s.check_call(cmd, shell=True)
        except s.CalledProcessError as error:
            with open("errors.txt", "a") as out:
                out.write(error.output)
                out.write("="*30+"\n")
    return


if __name__ == "__main__":
    main(argv[1])
