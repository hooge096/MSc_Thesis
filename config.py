#!/usr/bin/env python
"""
Author: Koen Hoogendoorn
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: all functions that deal with the config file

Instructions:
see run.py
"""

# Imports
import os
import collections
import subprocess as s

# Global variables
SCRIPT_PATH = os.sep.join(os.path.realpath(__file__).split(os.sep)[:-1])


def config_create():
    """
    Creates a config file in the script directory
    :return: str, path of the config file
    """

    # User information
    if not os.path.isfile(os.sep.join([SCRIPT_PATH, "config.txt"])):
        print "Config file does not exist, creating one now"
    else:
        print "Resetting config file to default settings"

    # All settings to be used for the config file
    config_sections = []
    busco_analysis_settings = collections.OrderedDict([("name", "BUSCO analysis settings"), ("--BUSCO_PATH", ""),
                                                       ("--lineage", "fungi"), ("--cpu", "Default"),
                                                       ("--ev", "Default"),
                                                       ("--mode", "genome"), ("-f", False),
                                                       ("--species", "Default"), ("--flanks", "Default"),
                                                       ("--size", "Default"), ("--long", False)])
    config_sections.append(busco_analysis_settings)
    antismash_analysis_settings = collections.OrderedDict([("name", "antiSMASH analysis settings"),
                                                           ("--clusterblast", False),
                                                           ("--subclusterblast", False), ("--knownclusterblast", True),
                                                           ("--smcogs", True), ("--inclusive", True),
                                                           ("--borderpredict", True), ("--full-hmmr", False),
                                                           ("--asf", False), ("--ecpred", "none"),
                                                           ("--modeling", "none"), ("--cpus", 8)])
    config_sections.append(antismash_analysis_settings)
    antismash_output_settings = collections.OrderedDict([("name", "antiSMASH output settings"), ("--outputfolder", ""),
                                                         ("--db", False), ("--disable-write_metabolicmodel", False),
                                                         ("--dboverwrite", False), ("--disable-svg", False),
                                                         ("--disable-xls", False), ("--disable-genbank", False),
                                                         ("--disable-embl", False), ("--disable-BiosynML", False),
                                                         ("--disable-html", False), ("--disable-txt", False)])
    config_sections.append(antismash_output_settings)
    cassis_analysis_settings = collections.OrderedDict([("name", "CASSIS analysis settings"),
                                                        ("--dir", "Default"), ("--fimo", "Default"),
                                                        ("--frequency", "Default"), ("--gap-length", 2),
                                                        ("--meme", "Default"), ("--num-cpus", 8),
                                                        ("--prediction", True), ("--verbose", True)])
    config_sections.append(cassis_analysis_settings)
    bigscape_analysis_settings = collections.OrderedDict([("name", "BiG-SCAPE network analysis settings"),
                                                          ("--BiGSCAPE_PATH", ""),
                                                          ("--outputdir", ""), ("--cores", 8),
                                                          ("--include_disc_nodes", False),
                                                          ("--domain_overlap_cutoff", 0.1),
                                                          ("--min_bgc_size", "Default"),
                                                          ("--seqdist_networks", "Default"),
                                                          ("--domaindist_network", "Default"),
                                                          ("--verbose", True)])
    config_sections.append(bigscape_analysis_settings)

    # Writes the config file

    with open(os.sep.join([SCRIPT_PATH, "config.txt"]), 'w') as out_handle:
        out_handle.write("##CONFIG V1.0\n\n")
        for name in config_sections:
            header = "##" + name["name"]+":\n"
            out_handle.write(header)
            for key, value in name.iteritems():
                if not key == "name":
                    out_handle.write(key+"="+str(value)+"\n")
            out_handle.write("\n")
    print "CONFIG file created at "+SCRIPT_PATH


def config_view():
    """
    Prints the config file on the screen
    :return: N/A
    """
    config_loc = os.sep.join([SCRIPT_PATH, "config.txt"])
    with open(config_loc, 'r') as in_handle:
        for line in in_handle:
            print line.rstrip()


def config_edit():
    """
    Attempts to open nano to edit the config file
    :return: N/A
    """
    filepath = os.sep.join([SCRIPT_PATH, "config.txt"])
    cmd = "nano {}".format(filepath)
    try:
        s.check_call(cmd, shell=True)
    except s.CalledProcessError:
        print "The default text editor is not installed on your system."
        print "If you wish to edit the config file, manually open the file and edit it."

if __name__ == "__main__":
    print "This is a module which only contains functions, and cannot be called on its own. Use run.py!"
