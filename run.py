#!/usr/bin/env python
"""
Author: Koen Hoogendoorn
Student number: 941124359110
Supervisors: Marnix Medema & Theo van der Lee
Script description: Framework for running various analysis tools involved in specialised metabolism gene clusters.

Dependencies:
-antiSMASH v3.0.5 (docker installation)
-BUSCO v1.1b1
-CASSIS v April 2016
-BioPython
-BCBio's GFF parser

Instructions:
    Use --help for extended information on all the script's functionality
"""

# IMPORTS
from __future__ import division

import argparse
import os.path
import subprocess as s
import config
import funclib
from funclib import check_valid_file, grab_filenames, timeit
import tools
# Global variables
# Classes & Functions


class _HelpAction(argparse._HelpAction):
    """
    Replaces the default help function of the main command 'python tools.py --help' with a more
    extensive version which includes the help messages of all submodules.
    """

    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_help()

        # retrieve subparsers from parser
        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, argparse._SubParsersAction)]
        # there will probably only be one subparser_action,
        # but better safe than sorry
        for subparsers_action in subparsers_actions:
            # get all subparsers and print help
            for choice, subparser in subparsers_action.choices.items():
                print("\nSubparser '{}'".format(choice))
                print(subparser.format_help())

        parser.exit()


def argparser():
    """
    Defines the permitted command line arguments of the script
    :return: NameSpace Object, contains all parsed command line arguments
    """
    wiki = "https://git.wageningenur.nl/hooge096/MSc_Thesis/wikis/home"
    # create the top-level parser
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-h", '--help', action=_HelpAction, help=wiki)
    subparsers = parser.add_subparsers(dest="command")

    # create the "busco" parser
    parser_a = subparsers.add_parser('busco')
    parser_a.add_argument('-i', '--input',
                          dest='busco',
                          type=check_valid_file,
                          help='The path to the file containing genome filepaths',
                          required=True)

    # create the "antismash" parser
    parser_b = subparsers.add_parser('antismash')
    parser_b.add_argument('-i', '--input',
                          dest='antismash',
                          type=check_valid_file,
                          help='The path to the file containing genome filepaths',
                          required=True)
    parser_b.add_argument('-v', '--version',
                          dest='version',
                          type=str,
                          choices=["dev", "official"],
                          default="dev",
                          help='The version of antiSMASH you want to run',
                          required=True)

    # create the "cassis" parser
    parser_c = subparsers.add_parser('cassis')
    parser_c.add_argument('-i', '--input',
                          dest='cassis',
                          type=check_valid_file,
                          help='The path to the file containing genome filepaths',
                          required=True)

    # create the "bigscape" parser
    parser_d = subparsers.add_parser('bigscape')
    parser_d.add_argument('-i', '--inputdir',
                          dest='bigscape',
                          type=check_valid_file,
                          help='The path to the directory containing all antiSMASH cluster files',
                          required=True)

    # create the "config" parser
    parser_e = subparsers.add_parser('config')
    configparse = parser_e.add_mutually_exclusive_group(required=True)
    configparse.add_argument("-v", "--view",
                             help="prints the config file to the screen",
                             action='store_true')
    configparse.add_argument("-r", "--restore",
                             help="restores the default settings of the config file",
                             action='store_true')
    configparse.add_argument("-e", "--edit",
                             help="opens the config file in a text editor so it can be edited",
                             action='store_true')

    # create the "compare" parser
    parser_f = subparsers.add_parser('compare')
    parser_f.add_argument('-i', '--inputdir',
                          dest='compare',
                          type=check_valid_file,
                          help='The path to the file containing the foldernames of the CASSIS & antiSMASH output. '
                               'They have to match, but this should automatically be the case.',
                          required=True)
    parser_f.add_argument('-w', '--ignore_warnings',
                          dest='warning',
                          help="ignore CASSIS warnings, and always take the first CASSIS prediction",
                          action='store_true')

    # create the "multigeneblast" parser
    parser_g = subparsers.add_parser('multigeneblast')
    parser_g.add_argument('-mgb', '--multigeneblast_path',
                          dest='path',
                          type=check_valid_file,
                          required=True)
    parser_g.add_argument('-db', '--database',
                          dest='db',
                          type=str,
                          required=True)
    parser_g.add_argument('-i', '--input',
                          dest='infile',
                          type=check_valid_file,
                          required=True)

    args = parser.parse_args()
    return args


@timeit
def multiple_input_handler(filelist, tool, warning=False):
    """
    Runs a tool multiple times for a list of files
    :param filelist:  list, paths to files as strings
    :param tool: str, tool identifier
    :param warning: bool, Use or ignore cassis warnings
    :return: N/A
    """
    busco_path = ""
    for inputfile in filelist:
        if tool == 'antismash_dev':
            tools.run_antismash(inputfile, 'dev')
        elif tool == 'antismash_official':
            tools.run_antismash(inputfile, 'official')
        elif tool == "busco":
            busco_path = tools.run_busco(inputfile, busco_path)
        elif tool == "cassis":
            tools.run_cassis(inputfile)
        elif tool == "comparison":
            outfolder = "."
            while not os.access(outfolder, os.W_OK):
                try:
                    s.check_call("mkdir {}".format(outfolder), shell=True)
                except s.CalledProcessError:
                    outfolder = raw_input("Comparison output folder: ")
                    outfolder = outfolder.strip()
                if not os.access(outfolder, os.W_OK):
                    print "Path not accessible, try another path."
            tools.compare_cassis_antismash(inputfile, outfolder, warning)


def program_flow():
    """
    Main program flow. Determines actions based on the arguments with which the tool was run
    :return: N/A
    """
    argument = argparser()

    if not os.path.isfile(config.SCRIPT_PATH+os.sep+"config.txt"):
        config.config_create()
    if argument.command == "config":
        if argument.edit:
            config.config_edit()
        if argument.view:
            config.config_view()
        if argument.restore:
            config.config_create()
    elif argument.command == "busco":
        if argument.busco:
            files = grab_filenames(argument.busco)
            multiple_input_handler(files, argument.command)
    elif argument.command == "antismash":
        if argument.antismash:
            if argument.antismash.endswith(".txt"):
                files = grab_filenames(argument.antismash)
            else:
                files = [argument.antismash]
            if argument.version == 'dev':
                multiple_input_handler(files, "antismash_dev")
            else:
                multiple_input_handler(files, "antismash_official")
    elif argument.command == "cassis":
        if argument.cassis:
            files = grab_filenames(argument.cassis)
            multiple_input_handler(files, argument.command)
    elif argument.command == "bigscape":
        if argument.bigscape:
            tools.run_bigscape(argument.bigscape)
    elif argument.command == "compare":
        if argument.compare:
            folders = grab_filenames(argument.compare)
            if argument.warning:
                multiple_input_handler(folders, "comparison", True)
            else:
                multiple_input_handler(folders, "comparison", False)
    elif argument.command == "multigeneblast":
        dbinput = funclib.move_clusters_to_folder()
        funclib.add_cluster_numbers(dbinput)
        tools.make_multigeneblast_db(argument.path, argument.db, dbinput)
        for filepath in funclib.extract_files(argument.infile):
            tools.clusterblast(filepath, argument.db, argument.path)

    else:
        print "Something went terribly wrong"


# Main
if __name__ == "__main__":
    program_flow()
