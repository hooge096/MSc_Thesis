#!/usr/bin/env python
# encoding: utf-8
"""
bed_from_genbank.py
grab the gene records from a genbank file (edit for other record types).
- requires:  biopython
"""

from Bio import SeqIO
from BCBio import GFF
import os
from sys import argv
import re
os.chdir(os.path.dirname(argv[0]))


def main():

    in_file = open("./Data/stringtie_test.gtf")
    out_file = open("./Data/stringtie_fixed.gtf", "w+")

    for line in in_file:

        if "reference_id" in line:
            print line
            try:
                gene = re.search(r"reference_id \"(.{5,12})\"", line).group(1)
            except AttributeError:
                continue
            else:
                print gene
                insertgene = '\"'+gene+'\"'
                newline = re.sub(r"\"STRG.[0-9]*\"", insertgene, line)
                out_file.write(newline)
        else:
            out_file.write(line)

        # print record.seq
        # GFF.write(record, out_file)
        # for feature in record.features:
        #     if feature.type == "transcript":
        #         try:
        #             feature.qualifiers["gene_id"] = feature.qualifiers["reference_id"]
        #             GFF.write(record, out_file)
        #             #print "check"
        #         except KeyError:
        #             continue
    in_file.close()
    out_file.close()

if __name__ == "__main__":
    main()
